# Proximi.io Android Library #

Welcome! You can use this library to hook into the Proximi.io platform.


## Getting started ##

* Clone this repository
* Copy proximiioandroidlibrary-release.aar into **libs** folder of your project/module
* Add following statement to top level of project/module **build.gradle** file 

```
	repositories {
		mavenCentral()
		flatDir {
			dirs "libs"
		}
	}

```

* Add following dependencies in **build.gradle**


```
	compile "com.android.support:appcompat-v7:22.2.0"
	compile "com.google.android.gms:play-services-base:7.5.0"
	compile "com.google.android.gms:play-services-location:7.5.0"
	compile 'com.android.support:multidex:1.0.0'
	compile ('com.firebase:geofire:1.1.0')  {
		exclude module: 'firebase-client-android'
		exclude module: 'firebase-client-jvm'
	}
	compile ('com.navtureapps.proximiiolibrary:proximiioandroidlibrary-release@aar') {
		exclude group: 'com.firebase', module: 'firebase-client-android'
	}
```


## Setup ##

Add the following sections to your **AndroidManifest.xml** file.

Permissions:

    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
    <uses-permission android:name="android.permission.CHANGE_NETWORK_STATE" />
    <uses-permission android:name="android.permission.WAKE_LOCK" />

Features:

    <uses-feature
     android:name="android.hardware.bluetooth_le"
     android:required="true" />

Services:

    <service android:name="com.navtureapps.proximiiosdk.ProximiioService" />
    <service android:name="com.navtureapps.proximiioandroid.ProximiioGeofenceService" />


## Settings ##

Application settings i.e iBeacon scan interval, native location services accuracy and other settings can be configured using the Proximi.io Web Portal (https://portal.proximi.io). Application settings can be found under Manage Applications -> Edit application -> Advanced. Settings are synced realtime to all application instances (requires a network connection).


## Usage ##

Create a new ProximiioListener, overriding at least the following methods.
This is an example, and you're free to use the callbacks however you wish.

    final Activity activity = this;
    ProximiioListener listener = new ProximiioListener() {
        @Override
        public void eventEnter(ProximiioInput input) {
			//Handle push messages
            input.handleOutput(activity, null, true, false);
        }

        @Override
        public void eventLeave(ProximiioInput input) {
			//Handle push messages
            input.handleOutput(activity, null, true, false);
        }

        @Override
        public void error(final Error error) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, "The following error occured: " + error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

Next, create a new instance of Proximi.io, storing it in an instance variable:

    private Proximiio proximiio;
    
    ...
    Settings.setAppID(this, "Your App ID here!");
    Settings.setAuthToken(this, "Your auth-token here!");
    proximiio = new Proximiio(this, Settings.getAppID(this), Settings.getAuthToken(this), listener);
 

Finally, override these methods in your activity:

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        proximiio.onActivityResult(requestCode, resultCode);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        proximiio.onDestroy();
    }

## That's it! ##

While this simple example only uses a single activity, Proximi.io is capable of transitioning between activities, and also likes to run on the background (see javadoc and Proximi.io Android Demo App).
You're now fully set to use Proximi.io on Android. While most of the application's settings are configured in the web portal, there's a lot more to master. Be sure to check out the rest of the methods in ProximiioListener!

For questions, email support@proximi.io.